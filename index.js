console.log('h')

//[SECTION] Arrays
	//Arrays are used to store multiple related values in a single variable.
	//They are deckared using square brackets([]) aka 'Array Literals/Square Brackets'

	let gradeNumberA= '98.5';
	let gradeNumberB= '98.5';
	let gradeNumberC= '98.5';
	let gradeNumberD= '98.5';

	let grades= [98.5, 94.3, 89.2, 90.1]
	let computerBrands= ['Acer', 'Asus', "Lenovo", 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
	let mixedArr= [12, 'Asus', null, undefined, {}];

	console.log(grades);
	console.log(computerBrands);
	console.log(mixedArr);

		//Alternative way to write arrays

		let myTasks= [
			'drink html',
			'eat javascript',
			'inhale css',
			'bake sass'
		];
		console.log(myTasks);

		let city1= 'Tokyo';
		let city2= 'Manila';
		let city3= 'Jakarta';

		let cities= [city1, city2, city3];

		console.log(cities);

	//[SECTION] Length property

	//the .length property allows us to get the set
	//of the total numberof items in an array.

	console.log(myTasks.length);

	let fullName= 'Voren Tejuco'
	console.log(fullName.length)
	// 12 characters including space

	//.length for variable counts characters while,
	//.length for arrays counts number of values/item

	myTasks.length= myTasks.length-1;
	console.log(myTasks.length);
	console.log(myTasks);

	//length property can also set the total# of
	//items in an array

	//another example using decrementation;
	cities.length --;
	console.log(cities);

	computerBrands.length -= 3;
	console.log(computerBrands);

	let theBeatles= ['John', 'Ringo', 'Paul', 'George'];
	theBeatles.length++
	console.log(theBeatles);

//[SECTION] Reading from Arrays
	//Accessing array elements is one of the more
	//common tasks that we do with an array.
	/*
	Syntax
		arrayname[index]
	*/

	console.log(grades[2]);
	console.log(computerBrands[4]);
	console.log(grades);

	//We can also reassign array values using
	//the items' indices

	let lakersLegends=['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem']
	console.log(lakersLegends[1]);
	console.log(lakersLegends[3]);

	let currentLaker= lakersLegends[2]
	console.log(currentLaker);


	//we can also reassign varray values using
	// the indices
	console.log('Array before reassignment');
	console.log(lakersLegends);
	lakersLegends[2]= 'Pau Gasol';
	console.log('Array after reassignment');
	console.log(lakersLegends);

	//subsection accesing the last elem of array

	let bullsLegends= ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
		console.log(bullsLegends.length);

	let lastElementIndex= bullsLegends.length-1;
	console.log(bullsLegends[lastElementIndex]);

	//or add it directly

	console.log(bullsLegends[bullsLegends.length-1]);

//[SUB-SECTION] Adding Items into the Array
	
	let newArr = [];
	console.log(newArr[0]);

	newArr[0]= 'Cloud Strife';
	console.log(newArr);

	newArr[1]= 'Tifa Lockhart';
	console.log(newArr);

	newArr[newArr.length]= 'Barret Wallace';
	console.log(newArr);

	//section looping over an array
	//u can use a for loop to iterate over all items
	//in an array

	for(let index= 0; index < newArr.length; index++){
		console.log(newArr[index]);
	};


let numArr= [5, 12, 30, 46, 40];

for (let index= 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index]+ ' is divisible by 5')

	} else {
		console.log(numArr[index]+ ' is not divisible by 5')
	}
};


//[SECTION] Multidimensional Arrays.

	//Multidimensional arrays are useful for storing complex data structures

	let chessBoard= [
		['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
	]

	console.log(chessBoard);

	//accessing elems of a multidimensional arrays
	console.log(chessBoard[1][4]); //result will be

	console.log('Pawn moves to: '+ chessBoard[1][4]);